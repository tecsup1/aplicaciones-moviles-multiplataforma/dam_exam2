import React, {Component, useState} from 'react';
import {
  Text,
  View,
  Button,
  TextInput,
  StyleSheet,
  Platform,
  Switch,
  Alert,
} from 'react-native';

import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';

class MaterialMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    const _menu = null;
  }

  

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  render() {
    return (
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <Menu
          ref={this.setMenuRef}
          button={
            <Text
              onPress={this.showMenu}
              style={{color: 'white', marginRight: 15}}>
              Menú
            </Text>
          }>
          <MenuItem
            onPress={this.props.signOut}>
            Cerrar sesión
          </MenuItem>
        </Menu>
      </View>
    );
  }
}

export default MaterialMenu;
