import React, { useEffect } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { Rating, AirbnbRating } from 'react-native-elements';
import { Card, ListItem, Button, Icon, Divider } from "react-native-elements";
import MapView, { PROVIDER_GOOGLE, Callout, Marker } from "react-native-maps";



const { width, height } = Dimensions.get("window");


export default ({ navigation, route }) => {
  const { nombre: restaurante } = route.params;

  useEffect(() => {
    navigation.setOptions({ title: route.params.nombre });
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.imagencontainer}>
          <Image
            source={{
              uri: `${route.params.imagen}`,
            }}
            style={{
              width: width,
              height: height / 3,
              flex: 1,
            }}
          />
        </View>
        <Card title={route.params.nombre}>
          <View style={{alignItems:"center"}}>
            <Text style={{fontWeight: "bold"}}>DIRECCIÓN:</Text>
            <Text style={{marginBottom: 8}}>{route.params.direccion}</Text>
           
            <Text style={{fontWeight: "bold"}}>ESTADO:</Text>
            <Text style={{marginBottom: 8}}>{route.params.estado}</Text>
            <Text style={{fontWeight: "bold"}}>TELEFONO:</Text>
            <Text style={{marginBottom: 8}}>{route.params.telefono}</Text>
            <Text style={{fontWeight: "bold"}}>RATING:</Text>
            <Rating imageSize={10} style={{width:47}} showRating ratingColor="#F44336" type="custom" fractions={1} startingValue={route.params.rating} />
  

            <TouchableOpacity
              style={[styles.button, { backgroundColor: "#FB5757" }]}
              onPress={() =>
                navigation.navigate("First", {
                  restaurante: route.params.nombre,
                })
              }
            >
              <Text
                style={{
                  textAlign: "center",
                  color: "white",
                  fontWeight: "bold",
                }}
              >
                RESERVAR
              </Text>
            </TouchableOpacity>
          </View>
        </Card>
        <Divider style={{ backgroundColor: 'gray', marginVertical: 18, height: 1.5}} />
        {/* <View style={styles.container2}>
          <Text>{route.params.nombre}</Text>
          <Text>Direccion: {route.params.direccion}</Text>
          <Text>Estado: {route.params.estado}</Text>
          <Text>Telefono: {route.params.telefono}</Text>
          <Text>Rating: {route.params.rating}</Text>
        </View> */}

        <View style={{ flex: 1, height: 400 }}>
          <Text style={{alignSelf: "center", fontSize: 16, fontWeight: "bold", marginBottom: 14}}>DONDE UBICARNOS</Text>
          <MapView
            style={{ height:"100%"}}
            provider={PROVIDER_GOOGLE}
            initialRegion={{
              latitude: route.params.latitud,
              longitude: route.params.longitud,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          >
            <Marker
              coordinate={{
                latitude: route.params.latitud,
                longitude: route.params.longitud,
              }}
            >
              <Callout>
                <View
                  style={{
                    borderRadius: 5,
                  }}
                >
                  <Text>{route.params.nombre}</Text>
                  <Image
                    source={{
                      uri: `${route.params.imagen}`,
                    }}
                    style={{ width: 77, height: 77 }}
                  />
                </View>
              </Callout>
            </Marker>
          </MapView>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imagencontainer: {
    width: width,
    height: height / 3,
  },
  container2: {
    width: width,
    height: 190,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "white",
    marginTop: 20,
    padding: 15,
  },
  button: {
    margin: 15,
    height: 40,
    justifyContent: "center",
    width: "100%",
    backgroundColor: "#428AF8",
    borderRadius: 10,
  },
});
