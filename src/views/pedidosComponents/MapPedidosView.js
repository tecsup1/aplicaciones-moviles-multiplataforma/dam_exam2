import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

class MapPedidoView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const {coordinates} = this.props.route.params.item;
    const item = this.props.route.params.item;
    console.log(item.url);
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          initialRegion={{
            // The latitude and longitude specifies the center location of the
            //  map and latitudeDelta and longitudeDelta specify the view
            //  area for map
            // latitude: -16.409046,
            // longitude: -71.537453,
            latitude: coordinates.latitude,
            longitude: coordinates.longitude,
            latitudeDelta: 0.0122,
            longitudeDelta: 0.0021,
          }}>
          <Marker
            // key={index}
            coordinate={{
              latitude: coordinates.latitude,
              longitude: coordinates.longitude,
            }}>
            <Image
                  source={{uri: 'https://pngimage.net/wp-content/uploads/2018/06/food-logo-png-2.png'}}
                  style={{width: 150, height: 150}}
                />
            <Callout>
              <View
                style={{
                  backgroundColor: '#FFFFFF',
                  borderRadius: 5,
                }}>
                <Text>{item.restaurant_name}</Text>
                <Image
                  source={{uri: item.url}}
                  style={{width: 250, height: 250}}
                />
              </View>
            </Callout>
          </Marker>
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default MapPedidoView;
