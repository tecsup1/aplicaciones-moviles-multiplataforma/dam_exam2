import React, { useState } from "react";
import { CommonActions } from "@react-navigation/native";
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Image,
} from "react-native";
import { auth } from "../../firebaConfig/firebasConfig";

const { width, height } = Dimensions.get("window");

export default function Registro({ navigation }) {
  const [form, useForm] = useState({
    email: "",
    password: "",
  });
  const sendData = async () => {
    try {
      await auth.createUserWithEmailAndPassword(form.email, form.password);
      navigation.navigate("Login");
    } catch (error) {
      alert(error);
    }

    // auth.createUserWithEmailAndPassword(form.email, form.password)
    //   .then(credencial => {
    //     navigation.navigate("Login")
    //   })
  };

  const volver = () => {
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'Login' },
        ],
      })
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titulo}>Formulario de Registro</Text>

      <TextInput
        style={styles.input}
        placeholder="Email"
        onChangeText={(text) => useForm({ ...form, email: text })}
      />

      <TextInput
        style={styles.input}
        placeholder="Contraseña"
        onChangeText={(text) => useForm({ ...form, password: text })}
      />

      {/* <TextInput style={styles.input} placeholder="Repita su contraseña" /> */}

      <TouchableOpacity style={styles.button}>
        <Text style={{ textAlign: "center" }} onPress={sendData}>
          Confirmar
        </Text>
      </TouchableOpacity>

      <TouchableOpacity style={[styles.button, { backgroundColor: "blue" }]}>
        <Text
          style={{ textAlign: "center", color: "white" }}
          onPress={volver}
        >
          Cancelar
        </Text>
      </TouchableOpacity>

      <Text style={{ fontSize: 13, color: "white" }}>
        No tienes una cuenta?
        <Text style={{ fontSize: 13, color: "#428AF8" }}>Registrate</Text>
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    flex: 1,
    backgroundColor: "black",
  },
  logo: {
    width: width,
    height: height / 3,
  },
  titulo: {
    marginTop: 20,
    fontSize: 23,
    color: "white",
  },
  input: {
    backgroundColor: "white",
    borderRadius: 10,
    marginVertical: 15,
    borderWidth: 2,
    textAlign: "center",
    color: "#428AF8",
    height: 37,
    width: width / 2,
  },
  button: {
    backgroundColor: "#F4D52A",
    borderRadius: 10,
    justifyContent: "center",
    width: width / 2,
    textAlign: "center",
    height: 37,
    marginBottom: 15,
  },
});
