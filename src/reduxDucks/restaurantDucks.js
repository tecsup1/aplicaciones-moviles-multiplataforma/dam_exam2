import * as pedidos from '../data/pedidos.json';
import {db2} from '../firebaConfig/firebasePedidosConfig';

// constantes
const dataInicial = {
  pedidosArray: [],
};

// types:
const OBTENER_PEDIDOS_EXITO = 'OBTENER_PEDIDOS_EXITO';
const SET_PEDIDO = 'SET_PEDIDO';
const DELETE_PEDIDO = 'DELETE_PEDIDO';

// reducer
export default function restaurantsReducer(state = dataInicial, action = {}) {
  switch (action.type) {
    case OBTENER_PEDIDOS_EXITO:
      return {...state, pedidosArray: action.payload};
    case SET_PEDIDO:
      return {...state, pedidosArray: [...state.pedidosArray, action.payload]};
    case DELETE_PEDIDO:
      const index = state.pedidosArray.findIndex(
        (n) => (n.id = action.payload),
      );
      state.pedidosArray.splice(index, 1);
      return {
        ...state,
      };
    default:
      return state;
  }
}

//acciones
export const obtenerPedidosAction = () => async (dispatch, getState) => {
  const root = await db2.collection('pedidos').get();
  // const pedidos = root.docs.map((e) => e.data());
  const pedidos = root.docs.map((e) => {
    return {id: e.id, ...e.data()};
  });
  // console.log('pedidos papu: ', pedidos);

  dispatch({
    type: 'OBTENER_PEDIDOS_EXITO',
    payload: pedidos,
  });
};

export const setPedidoAction = (payload) => async (dispatch, getState) => {
  console.log('payload papu: ', payload);
  try {
    await db2.collection('pedidos').add(payload);
  } catch (e) {
    console.log('error papu: ', e);
  }
  dispatch({
    type: 'SET_PEDIDO',
    payload: payload,
  });
};

export function deletePedidoAction(id) {
  return async function (dispatch, getState) {
    try {
      // console.log('@@@@@@@@@@', id);
      await db2.collection('pedidos').doc(id).delete();
      dispatch({
        type: 'DELETE_RESERVA',
        payload: id,
      });
    } catch (error) {
      console.log(error);
    }
  };
}
