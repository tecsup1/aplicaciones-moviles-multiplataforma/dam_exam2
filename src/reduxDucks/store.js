import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reducer from "./reducer";
import restaurantDucks from "./restaurantDucks";

const root = combineReducers({
  reducer,
  restaurantDucks,
});
const store = createStore(root, applyMiddleware(thunk));

export default store;
